from flask import Flask

app = Flask(__name__)

DOG_FACTS = [
"Endal was the first dog to ride on the London Eye (the characteristic ferris wheel in London, England), and was also the first known dog to successfully use a ATM machine.",
"At the age of 4 weeks, most dogs have developed the majority of their vocalizations."
]

@app.route("/")
def index():
    return "Hello World!"

if __name__ == "__main__":
    app.run()



